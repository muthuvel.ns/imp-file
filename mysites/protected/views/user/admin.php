<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		'firstname',
		'password',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php if($model != null): ?>
<ul>
    <?php 
$productAttributes = $model->getAttributes($model->safeAttributeNames);

$names = User::model()->getAttributes(array('id')); 
//	echo'<pre>';print_r($names);exit;
foreach($productAttributes as $key=>$models): ?>
    <li><?php echo $key; ?></li>
    <li><?php echo $models; ?></li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>

<div>
	<table style="width:100%" border="1">
  <tr>
    <td>Username</td>
    <td>Firstname</td>		
    <td>Edit</td>
  </tr>
		
	<?php //$data	=	(array)$model;
//echo'<pre>';print_r($model);exit;
	foreach($model as $key=>$val){ //var_dump($val->attributes);?>	
  <tr>
    <td><?php echo $key; ?></td>
    <td><?php //echo $val->attributes; ?></td>		
    <td>Edit</td>
  </tr>
 <?php } ?>
</table>
</div>
