<?php
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'book-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'bookname'); ?>
		<?php echo $form->textField($model,'bookname',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'bookname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textarea($model,'content'); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'Publish year'); ?>
		<?php //echo $form->textField($model,'dob'); 
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					  'model' => $model,
					  'attribute' => 'publish_year',
					  'htmlOptions' => array(
					//   'size' => '10',         // textField size
					   'maxlength' => '10',    // textField maxlength
					  ),
					  'options'=>array(
							  'startDate'=>date("yy-mm-dd"),
							  //'endDate'=>date("yy-mm-dd"),
							  'maxDate'=>'0',  // this will disable previous dates from datepicker
							  'showAnim'=>'fold',
							  'dateFormat'=>'yy-mm-dd',
							  'changeMonth'=>'true',
							  'changeYear'=>'true',
							  'yearRange'=>'1940:2100',),
					 ));
		?>
		<?php echo $form->error($model,'publish_year'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'Photo'); ?>
		<?php echo $form->fileField($model,'photo'); ?>
		<?php echo $form->error($model,'photo'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'Author'); ?>
		<?php echo $form->textField($model,'author'); ?>
		<?php echo $form->error($model,'author'); ?>
	</div>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->