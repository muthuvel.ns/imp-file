<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Book'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Of Books', 'url'=>array('index')),
	array('label'=>'Manage Book', 'url'=>array('manage')),
);
?>

<h1>Create Book</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>