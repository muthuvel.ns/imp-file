<script type="text/javascript" src="/mysites/assets/e502186f/jquery.js"></script>
<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Book-'.Yii::app()->session['userid'],
);

$this->menu=array(
	array('label'=>'Create Books', 'url'=>array('create')),
	array('label'=>'Manage Book', 'url'=>array('manage')),
	array('label'=>'Logout', 'url'=>array('logout')),
);
?>


<?php
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
