<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Email/Username'); ?>
		<?php echo $form->textField($model,'username',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Repeat Password'); ?>
		<?php echo $form->passwordField($model,'repeat_password',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'Repeat Password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dob'); ?>
		<?php //echo $form->textField($model,'dob'); 
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					  'model' => $model,
					  'attribute' => 'dob',
					  'htmlOptions' => array(
					   'size' => '10',         // textField size
					   'maxlength' => '10',    // textField maxlength
					  ),
					  'options'=>array(
							  'startDate'=>date("yy-mm-dd"),
							  //'endDate'=>date("yy-mm-dd"),
							  'maxDate'=>'0',  // this will disable previous dates from datepicker
							  'showAnim'=>'fold',
							  'dateFormat'=>'yy-mm-dd',
							  'changeMonth'=>'true',
							  'changeYear'=>'true',
							  'yearRange'=>'1940:2100',),
					 ));
		?>
		<?php echo $form->error($model,'dob'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'photo'); ?>
		<?php echo $form->fileField($model,'photo'); ?>
		<?php echo $form->error($model,'photo'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address'); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone'); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Register' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->