<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		//echo'<pre>';print_r(Yii::app()->user);exit;
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','remove'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin','demo'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		
		 
		/**
			// Importing ExportXLS class file
 
				Yii::Import('application.extensions.ExportXLS.ExportXLS');

				// Xls Header Row
				$headercolums =array('Name','age'); 

				// Xls Data
				$row=array(array('Sachin','35'),array('sehwag',30));

				// Xls File Name
				$filename = 'fileName.xls';
					$xls      = new ExportXLS($filename);
					$header = null;
					$xls->addHeader($headercolums);
					$xls->addRow($row);
					$xls->sendFile();
				
		**/
		
		/**
					Yii::import('ext.phpexcel.XPHPExcel');    
				  $objPHPExcel= XPHPExcel::createPHPExcel();
				  $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("Maarten Balliauw")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");
 
 
					// Add some data
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A1', 'Hello')
								->setCellValue('B2', 'world!')
								->setCellValue('C1', 'Hello')
								->setCellValue('D2', 'world!');

					// Miscellaneous glyphs, UTF-8
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A4', 'Miscellaneous glyphs')
								->setCellValue('A5', 'Ã©Ã Ã¨Ã¹Ã¢ÃªÃ®Ã´Ã»Ã«Ã¯Ã¼Ã¿Ã¤Ã¶Ã¼Ã§');

					// Rename worksheet
					$objPHPExcel->getActiveSheet()->setTitle('Simple');


					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);


					// Redirect output to a clientâ€™s web browser (Excel5)
					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: attachment;filename="01simple.xls"');
					header('Cache-Control: max-age=0');
					// If you're serving to IE 9, then the following may be needed
					header('Cache-Control: max-age=1');

					// If you're serving to IE over SSL, then the following may be needed
					header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
					header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
					header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
					header ('Pragma: public'); // HTTP/1.0

					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$objWriter->save('php://output');
						  Yii::app()->end();
		**/
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//echo'<pre>';print_r($_POST['User']);exit;
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save()) {
				$user_id	=	$model->id;
				$profile	=	$_POST['User'];
				
				
				// Importing ExportXLS class file
 
				Yii::Import('application.extensions.ExportXLS.ExportXLS');

				// Xls Header Row
				$headercolums =array('Name','age'); 

				// Xls Data
				$row=array(array('Sachin','35'),array('sehwag',30));

				// Xls File Name
				$filename = 'fileName.xls';
					$xls      = new ExportXLS($filename);
					$header = null;
					$xls->addHeader($headercolums);
					$xls->addRow($row);
					$xls->sendFile();
				
				
				$rnd = rand(0,9999);  // generate random number between 0-9999
				$uploadedFile=CUploadedFile::getInstance($model,'photo');
				$fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
				$model->photo = $fileName;

				
				 $uploadedFile->saveAs(Yii::app()->basePath.'/banner/'.$fileName);  // image will uplode to rootDirectory/banner/
				
					$command = Yii::app()->db->createCommand();
					$command->insert(	'profile', array(
										'dob'=>$profile['dob'],
										'address'=>$profile['address'],
										'photo'=>$model->photo,
										'phone'=>$profile['phone'],
										'user_id'=>$user_id
									));
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionRemove($id)
	{ 
		//echo'<pre>';print_r($_REQUEST);exit;
	 $this->loadModel($id)->delete();
		
		
		

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		// $criteria = new CDbCriteria;
        //    $criteria->select = 't.*, tu.* ';
         //   $criteria->join = ' LEFT JOIN `profile` AS `tu` ON t.id = tu.user_id';
            //$criteria->addCondition("display_name LIKE '%a%' and blocked_by='76'");
            			
		$dataProvider=User::model()->findAll();
		//$dataProvider=new CActiveDataProvider('User');
		//$usersArr = CHtml::listData( $dataProvider, 'id' , 'username','photo');

		//echo'<pre>';print_r($usersArr);exit;
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
