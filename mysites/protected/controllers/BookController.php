<?php

class BookController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	public $example;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
	//	echo'<pre>';print_r(Yii::app()->user);exit;
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Logout'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('remove'),
				'users'=>array('@'),
			),
			array('allow', // allow only the owner to perform 'view' 'update' 'delete'                                       actions
                'actions' => array('view', 'update', 'delete','manage','create'),
               'expression' => "( Yii::app()->session['userid']!='kevell' )",
            ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
 	public function allowOnlyOwner(){
 		
    
            $example = User::model()->find("username='".Yii::app()->session['userid']."'");
         //	   echo'<pre>';print_r($example->id);exit;
            return true;
    }
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Book;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Book']))
		{
			//echo'<pre>';print_r($_FILES);exit;
			if (isset( $_FILES['Book'] ) && $_FILES['Book']['error']['photo']==0){
				$rnd = rand(0,9999);  // generate random number between 0-9999
				$uploadedFile=CUploadedFile::getInstance($model,'photo');
				$fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
				$model->photo = $fileName;

			
				$uploadedFile->saveAs(Yii::app()->basePath.'/banner/'.$fileName);  // image will uplode to rootDirectory/banner/
				$model->attributes=$model->photo;
			}
			
			$model->attributes=$_POST['Book'];
			//echo'<pre>';print_r($model);exit;
			if($model->save()) {
			/*	$user_id	=	$model->id;
				$profile	=	$_POST['Book'];
				
				$rnd = rand(0,9999);  // generate random number between 0-9999
				$uploadedFile=CUploadedFile::getInstance($model,'photo');
				$fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
				$model->photo = $fileName;

				
				 $uploadedFile->saveAs(Yii::app()-Yii::app()->user->name>basePath.'/banner/'.$fileName);  // image will uplode to rootDirectory/banner/
				
					$command = Yii::app()->db->createCommand();
					$command->insert(	'profile', array(
										'dob'=>$profile['dob'],
										'address'=>$profile['address'],
										'photo'=>$model->photo,
										'phone'=>$profile['phone'],
										'user_id'=>$user_id
									));*/
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' publish_yearpage.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Book']))
		{
			$model->attributes=$_POST['Book'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionRemove($id)
	{
		//echo'<pre>';print_r($_REQUEST);exit;
	 $this->loadModel($id)->delete();
		
		
		

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		echo'<pre>';print_r(Yii::app()->session['userid']);exit;	
		
		if (!isset( Yii::app()->session['userid'] )){
			$this->redirect(array('site/index'));
		}
		 //Yii::app()->user->name	=	Yii::app()->user->name; 
			
	//	$criteria = new CDbCriteria;
     //   $criteria->select = 't.*, tu.* ';
         //   $criteria->join = ' LEFT JOIN `profile` AS `tu` ON t.id = tu.user_id';
            //$criteria->addCondition("display_name LIKE '%a%' and blocked_by='76'");
      /*$dataProvider = Yii::app()->db->createCommand()
					    ->select('*')
					    ->from('books_details')
					    ->queryAll();
					    
			    echo'<pre>';print_r($dataProvider);exit;*/
		//$dataProvider=Book::model()->findAll();
		$dataProvider=new CActiveDataProvider('Book');
		
		//$usersArr = CHtml::listData( $dataProvider, 'id' , 'username','photo');

		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionManage()
	{
		$model=new Book('search');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Book']))
			$model->attributes=$_GET['Book'];

		$this->render('manage',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Book::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		
		if(isset(Yii::app()->session['userid']))
		{
		        unset(Yii::app()->session['userid']);
		}
		Yii::app()->user->logout();
		Yii::app()->session->destroy();
	
		$this->redirect(Yii::app()->homeUrl);
	}
}
